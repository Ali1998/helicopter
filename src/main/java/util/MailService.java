package util;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class MailService {

    private static final Logger logger = Logger.getLogger(MailService.class);

    private static final Properties mailProperties = new Properties();


    static {

        mailProperties.setProperty("mail.smtp.host", "smtp.yandex.ru");
        mailProperties.setProperty("mail.smtp.auth", "true");
        mailProperties.setProperty("mail.smtp.port", "465");
        mailProperties.setProperty("mail.smtp.starttls.enable", "true");
        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    }


    private static Session getSmtpSession() {

        try {
            return Session.getInstance(mailProperties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("alis@crocusoft.com", "test123");
                }
            });

        } catch (Exception e) {
            logger.error("Error while creating SMTP Session ", e);
        }
        return null;
    }


    public boolean sendMail(String subject, String content, String mailTo) {


        try {

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(getSmtpSession());

            // Set From: header field of the header.
            message.setFrom(new InternetAddress("alis@crocusoft.com"));

            // Set To: header field of the header.
            message.addRecipients(Message.RecipientType.TO, mailTo);

            // Set Subject: header field
            message.setSubject(subject, "utf-8");

            // Now set the actual message
            message.setContent(content, "text/html; charset=utf-8");
            //message.setText(content, "utf-8");

            // Send message
            Transport.send(message);
            logger.info("Mail successfully sent to " + mailTo);

            return true;

        } catch (Exception ex) {
            logger.error("Error while sending Mail ", ex);
        }

        return false;
    }

}
