package util;




import org.apache.commons.codec.binary.Base64;
import sun.rmi.runtime.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.regex.Pattern;

import static org.apache.commons.codec.binary.Base64.*;


public class Methods {




    public static boolean isNumber (String number)
    {
        String regex="\\d+";
        Pattern p=Pattern.compile(regex);
        return p.matcher(number).matches();
    }



    public static String generateRandom(){
        Random r=new Random();
        int rand=r.nextInt(100000)+100000+r.nextInt(800000);
        return String.valueOf(rand);
    }



//
//    public static String getBase64EncodedImage(String imageURL) throws IOException {
//        URL url = new URL(imageURL);
//        InputStream is = url.openStream();
//        byte[] bytes = org.apache.commons.io.IOUtils.toByteArray(is);
//        return Base64.encodeBase64String(bytes);
//    }



    public static String getBase64EncodedImage(String url) {

        try {
            URL imageUrl = new URL(url);
            URLConnection ucon = imageUrl.openConnection();
            InputStream is = ucon.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[256];
            int read = 0;
            while ((read = is.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, read);
            }
            baos.flush();
            return Base64.encodeBase64String(baos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }








}
