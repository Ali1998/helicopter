package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonInitializer {

    private static Gson instance;

    private GsonInitializer(){}

    public static synchronized Gson getInstance(){
        if(instance == null){
            instance = new GsonBuilder().setPrettyPrinting().create();
        }
        return instance;
    }
}