package model;

import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.DefaultValue;

public class SchemePart {

    private Integer schemaId;
    private Integer schemaPartId;
    private String schemaPartNameEn;
    private String schemaPartNameRu;
    private String schemaPartDescEn;
    private String schemaPartDescRu;

    public String getSchemaPartDescEn() {
        return schemaPartDescEn;
    }

    public void setSchemaPartDescEn(String schemaPartDescEn) {
        this.schemaPartDescEn = schemaPartDescEn;
    }

    public String getSchemaPartDescRu() {
        return schemaPartDescRu;
    }

    public void setSchemaPartDescRu(String schemaPartDescRu) {
        this.schemaPartDescRu = schemaPartDescRu;
    }

    public Integer getSchemaPartId() {
        return schemaPartId;
    }

    public void setSchemaPartId(Integer schemaPartId) {
        this.schemaPartId = schemaPartId;
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public String getSchemaPartNameEn() {
        return schemaPartNameEn;
    }

    public void setSchemaPartNameEn(String schemaPartNameEn) {
        this.schemaPartNameEn = schemaPartNameEn;
    }

    public String getSchemaPartNameRu() {
        return schemaPartNameRu;
    }

    public void setSchemaPartNameRu(String schemaPartNameRu) {
        this.schemaPartNameRu = schemaPartNameRu;
    }


}
