package model;

public class Constant {

    public static final Integer INTERNAL_EXCEPTION=2000;
    public static final Integer WrongTransaction=1006;
    public static final Integer SUCCESS=1000;
    public static final Integer MISSED_PARAM=1008;
    public static final Integer NoInformation=1014;
    public static final Integer DuplicateKey=1019;
    public static final Integer EmptyData=1004;
    public static final Integer NotConfirmed=1007;
    public static final Integer WrongCode=1012;
    public static final Integer Approved=1003;
    public static final Integer NotCurrentData=1005;
    public static final Integer Session=1032;
    public static final Integer WrongParam=1025;
    public static final Integer WrongLength=1023;
    public static final Integer WrongPassword=1015;



}
