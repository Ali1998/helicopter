package model;

import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.DefaultValue;

public class Schemes {

    private Integer helicopterId;
    private String schemaNameEn;
    private String schemaNameRu;
    private Integer schemaId;
    private Integer parentId;

    public Integer getHelicopterId() {
        return helicopterId;
    }

    public void setHelicopterId(Integer helicopterId) {
        this.helicopterId = helicopterId;
    }

    public String getSchemaNameEn() {
        return schemaNameEn;
    }

    public void setSchemaNameEn(String schemaNameEn) {
        this.schemaNameEn = schemaNameEn;
    }

    public String getSchemaNameRu() {
        return schemaNameRu;
    }

    public void setSchemaNameRu(String schemaNameRu) {
        this.schemaNameRu = schemaNameRu;
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
