package model;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import util.GsonInitializer;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Properties;



public class Config {


    private static final Logger logger = LogManager.getLogger(Config.class);

    public static final String CONFIG_FILE_PATH = "message.properties";

    public static final String HEADER=get("header");

    public static final String TRANSACTION=get("transaction");

    public static final String BODY=get("body");

    public static final String SUBJECT=get("subject");







    private static Properties properties;

    public static String get(String keyword) {
        if (properties == null) {
            properties = getProperties();
            if (properties == null) {
                return null;
            }
        }
        return properties.getProperty(keyword);
    }

    private static Properties getProperties() {
        InputStream fis = null;
        try {

            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Properties props = new Properties();

            try {
                InputStream resourceStream = loader.getResourceAsStream(CONFIG_FILE_PATH);
                props.load(resourceStream);

            } catch (Exception e) {
                throw new IOException();
            }

            return props;

        } catch (IOException ex) {
            try {
                Properties property = new Properties();
                property.load(new FileInputStream("./config/" + CONFIG_FILE_PATH));

                return property;
            } catch (IOException exc) {
                logger.error("There is problem in reading Properties file ", ex);
                return null;
            }
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                logger.error("There is problem in closing FileInputStream", ex);
            }
        }
    }




}
