package model;

import com.fasterxml.jackson.annotation.*;
import javafx.beans.DefaultProperty;

public class Bulletin {

    private Integer id;
    private String  nameEn;
    private String  nameRu;
    private String  descEn;
    private String  descRu;
    private String  publishDate;
    private String  modelEn;
    private String  modelRu;
    private String  partRu;
    private String  partEn;
    private String  shortDescEn;
    private String  shortDescRu;
    private Integer helicopterId;


    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Integer getHelicopterId() {
        return helicopterId;
    }

    public void setHelicopterId(Integer helicopterId) {
        this.helicopterId = helicopterId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescRu() {
        return descRu;
    }

    public void setDescRu(String descRu) {
        this.descRu = descRu;
    }

    public String getModelEn() {
        return modelEn;
    }

    public void setModelEn(String modelEn) {
        this.modelEn = modelEn;
    }

    public String getModelRu() {
        return modelRu;
    }

    public void setModelRu(String modelRu) {
        this.modelRu = modelRu;
    }

    public String getPartRu() {
        return partRu;
    }

    public void setPartRu(String partRu) {
        this.partRu = partRu;
    }

    public String getPartEn() {
        return partEn;
    }

    public void setPartEn(String partEn) {
        this.partEn = partEn;
    }

    public String getShortDescEn() {
        return shortDescEn;
    }

    public void setShortDescEn(String shortDescEn) {
        this.shortDescEn = shortDescEn;
    }

    public String getShortDescRu() {
        return shortDescRu;
    }

    public void setShortDescRu(String shortDescRu) {
        this.shortDescRu = shortDescRu;
    }
}
