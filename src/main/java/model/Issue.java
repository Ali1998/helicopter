package model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Value;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Issue {

    private Integer issueId;
    private String  transactionId;
    private String  message;
    private Integer schemaPartId;
    private String issueNameEn;
    private String issueNameRu;
    private String bodyIssueEn;
    private String bodyIssueRu;
    private String bodyCommentEn;
    private String bodyCommentRu;
    private String bodySolveEn;
    private String bodySolveRu;


    public Integer getSchemaPartId() {
        return schemaPartId;
    }

    public void setSchemaPartId(Integer schemaPartId) {
        this.schemaPartId = schemaPartId;
    }

    public String getIssueNameEn() {
        return issueNameEn;
    }

    public void setIssueNameEn(String issueNameEn) {
        this.issueNameEn = issueNameEn;
    }

    public String getIssueNameRu() {
        return issueNameRu;
    }

    public void setIssueNameRu(String issueNameRu) {
        this.issueNameRu = issueNameRu;
    }

    public String getBodyIssueEn() {
        return bodyIssueEn;
    }

    public void setBodyIssueEn(String bodyIssueEn) {
        this.bodyIssueEn = bodyIssueEn;
    }

    public String getBodyIssueRu() {
        return bodyIssueRu;
    }

    public void setBodyIssueRu(String bodyIssueRu) {
        this.bodyIssueRu = bodyIssueRu;
    }

    public String getBodyCommentEn() {
        return bodyCommentEn;
    }

    public void setBodyCommentEn(String bodyCommentEn) {
        this.bodyCommentEn = bodyCommentEn;
    }

    public String getBodyCommentRu() {
        return bodyCommentRu;
    }

    public void setBodyCommentRu(String bodyCommentRu) {
        this.bodyCommentRu = bodyCommentRu;
    }

    public String getBodySolveEn() {
        return bodySolveEn;
    }

    public void setBodySolveEn(String bodySolveEn) {
        this.bodySolveEn = bodySolveEn;
    }

    public String getBodySolveRu() {
        return bodySolveRu;
    }

    public void setBodySolveRu(String bodySolveRu) {
        this.bodySolveRu = bodySolveRu;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }
}
