package model;

public class ApplicationData {

    private String aboutEn;
    private String aboutRu;
    private String phoneEn;
    private String phoneRu;
    private String emailEn;
    private String emailRu;

    public String getAboutEn() {
        return aboutEn;
    }

    public void setAboutEn(String aboutEn) {
        this.aboutEn = aboutEn;
    }

    public String getAboutRu() {
        return aboutRu;
    }

    public void setAboutRu(String aboutRu) {
        this.aboutRu = aboutRu;
    }

    public String getPhoneEn() {
        return phoneEn;
    }

    public void setPhoneEn(String phoneEn) {
        this.phoneEn = phoneEn;
    }

    public String getPhoneRu() {
        return phoneRu;
    }

    public void setPhoneRu(String phoneRu) {
        this.phoneRu = phoneRu;
    }

    public String getEmailEn() {
        return emailEn;
    }

    public void setEmailEn(String emailEn) {
        this.emailEn = emailEn;
    }

    public String getEmailRu() {
        return emailRu;
    }

    public void setEmailRu(String emailRu) {
        this.emailRu = emailRu;
    }
}
