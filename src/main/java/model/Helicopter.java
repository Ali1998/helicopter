package model;

import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.DefaultValue;

public class Helicopter {

    private Integer   helicopterId;
    private String    helicopterNameEn;
    private String    helicopterNameRu;

    public Integer getHelicopterId() {
        return helicopterId;
    }

    public void setHelicopterId(Integer helicopterId) {
        this.helicopterId = helicopterId;
    }

    public String getHelicopterNameEn() {
        return helicopterNameEn;
    }

    public void setHelicopterNameEn(String helicopterNameEn) {
        this.helicopterNameEn = helicopterNameEn;
    }

    public String getHelicopterNameRu() {
        return helicopterNameRu;
    }

    public void setHelicopterNameRu(String helicopterNameRu) {
        this.helicopterNameRu = helicopterNameRu;
    }
}
