package handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

//    @Autowired
//    private HomeService homeService;

    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        User authUser = (User) authentication.getPrincipal();
//        userService.setSessionAttributes(httpServletRequest, authUser.getUsername());
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/dashboard");
    }
}
