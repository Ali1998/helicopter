package dao.Impl;

import dao.AllDataDao;
import dao.SqlQueries;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import util.Methods;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AllDataDaoImpl implements AllDataDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource dataSource) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Helicopter> getHelicopterList() {
        return jdbcTemplate.query(SqlQueries.GET_HELICOPTER_LIST, BeanPropertyRowMapper.newInstance(Helicopter.class));
    }

    @Override
    public List<Schemes> getSchemes() {
       return jdbcTemplate.query(SqlQueries.GET_SCHEMES_DATA,BeanPropertyRowMapper.newInstance(Schemes.class));
    }

    @Override
    public List<SchemePart> getSchemeParts() {
        return jdbcTemplate.query(SqlQueries.GET_SCHEME_PART_DATA,BeanPropertyRowMapper.newInstance(SchemePart.class));
    }

    @Override
    public List<Issue> getIssueList() {
        return jdbcTemplate.query(SqlQueries.GET_ISSUE_LIST, BeanPropertyRowMapper.newInstance(Issue.class));
    }

    @Override
    public List<Bulletin> getBulletens() {
        return jdbcTemplate.query(SqlQueries.GET_BULLETENS_LIST,BeanPropertyRowMapper.newInstance(Bulletin.class));
    }

    @Override
    public List<Images> getImagesList() {
        List<Images>images=jdbcTemplate.query(SqlQueries.GET_IMAGES_LIST,BeanPropertyRowMapper.newInstance(Images.class));
        for(Images image:images){

            String imageUrl=Config.get("image_url")+image.getImageUrl();
            try{
                image.setImageUrl(Methods.getBase64EncodedImage(imageUrl));
            }catch (Exception ex){
                image.setImageUrl("");
            }

        }
        return images;
    }

    @Override
    public List<BulletinImages> getBulletinImages() {
        List<BulletinImages>images=jdbcTemplate.query(SqlQueries.GET_BULLETEN_IMAGE_LIST,BeanPropertyRowMapper.newInstance(BulletinImages.class));
        for(BulletinImages image:images){
            String imageUrl=Config.get("image_url")+image.getImageUrl();
            try{
                image.setImageUrl(Methods.getBase64EncodedImage(imageUrl));
            }catch (Exception ex){
                image.setImageUrl("");
            }

        }
        return images;
    }

    @Override
    public List<Map<String, Object>> getApplicationDatas(){
        return jdbcTemplate.queryForList(SqlQueries.APPLICATION_DATA,new MapSqlParameterSource());
    }

    @Override
    public boolean addDescription(SchemaBulletenDesc desc) {

        if(desc.getType()==1){
            return jdbcTemplate.update(SqlQueries.ADD_SCHEMA_DESC,new BeanPropertySqlParameterSource(desc))>0;
        }else if(desc.getType()==2){
            return jdbcTemplate.update(SqlQueries.ADD_BULLETEN_DESC,new BeanPropertySqlParameterSource(desc))>0;
        }
        return false;
    }


}
