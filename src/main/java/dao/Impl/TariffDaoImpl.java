package dao.Impl;

import dao.SqlQueries;
import dao.TariffDao;
import model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;


@Repository
public class TariffDaoImpl implements TariffDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource dataSource) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Tariff> getTariffList(String transactionId) {
        return jdbcTemplate.query(SqlQueries.GET_TARIFF_LIST,BeanPropertyRowMapper.newInstance(Tariff.class));
    }

    @Override
    public Tariff tariff(int trfId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("id",trfId);
        return jdbcTemplate.queryForObject(SqlQueries.GET_PACKAGES_BY_ID,param,BeanPropertyRowMapper.newInstance(Tariff.class));
    }

    @Override
    public boolean buyTariff(Tariff tariff,String transactionId,Integer id) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        param.addValue("tariffId",tariff.getId());
        param.addValue("timeCount",tariff.getMonth());
        param.addValue("price",tariff.getAmount());
        param.addValue("id",id);
        boolean result=jdbcTemplate.queryForObject(SqlQueries.CHECK_USER_TARIFF,param,Integer.class)>0;
        if(result){
            return jdbcTemplate.update(SqlQueries.EDIT_BUY_TARIFF,param)>0;
        }else{
            return jdbcTemplate.update(SqlQueries.BUY_TARIFF,param)>0;
        }

    }

    @Override
    public String tariffDate(String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("trId",transactionId);
        return jdbcTemplate.queryForObject(SqlQueries.GET_USER_TARIFF_FINISH_DATE,param,String.class);
    }

    @Override
    public boolean checkUserTariff(String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        String sql=SqlQueries.CHECK_USER_TARIFF+" and ut.finish_date>now()";
        return jdbcTemplate.queryForObject(sql,param,Integer.class)>0;
    }


}
