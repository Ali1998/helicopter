package dao.Impl;

import dao.LoginDao;
import dao.SqlQueries;
import model.Issue;
import model.Login;
import model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Repository
public class LoginDaoImpl implements LoginDao {


    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource dataSource) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public boolean addRegister(Profile profile) {
        return jdbcTemplate.update(SqlQueries.REGISTRATION,new BeanPropertySqlParameterSource(profile))>0;
    }

    @Override
    public Login checkLogin(Login login) {

        boolean res=jdbcTemplate.queryForObject(SqlQueries.CHECK_LOGIN,
                new BeanPropertySqlParameterSource(login),Integer.class)>0;
        if(res){
            return jdbcTemplate.queryForObject(SqlQueries.GET_LOGIN,
                    new BeanPropertySqlParameterSource(login),BeanPropertyRowMapper.newInstance(Login.class));
        }else{
            return null;
        }

    }

    @Override
    public boolean checkUser(String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_USER,param,Integer.class)>0;
    }

    @Override
    public boolean checkMail(String mail,int type) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        String sql=SqlQueries.CHECK_USER_BY_MAIL;
        if(type==1){
            sql+=" and status=1";
            return jdbcTemplate.queryForObject(sql,param,Integer.class)>0;
        }else if(type==2){
            sql+=" and (status=1 or status=2)";
            return jdbcTemplate.queryForObject(sql,param,Integer.class)>0;

        }else{
            return false;
        }
    }


    @Override
    public Map<String,Object> getProfile(String transactionId) {
        Map<String,Object>map;
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        map=jdbcTemplate.queryForMap(SqlQueries.GET_PROFILE,param);
        map.put("country",jdbcTemplate.queryForMap(SqlQueries.GET_COUNTRY,param));
        return map;
    }

    @Override
    public boolean confirmUser(String mail, String token) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        param.addValue("token",token);
        boolean result=jdbcTemplate.queryForObject(SqlQueries.CONFIRM_USER,param,Integer.class)>0;
        if(result){
           return jdbcTemplate.update(SqlQueries.EDIT_STATUS,param)>0;
        }else{
            return false;
        }

    }

    @Override
    public Login getUser(String mail) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        return jdbcTemplate.queryForObject(SqlQueries.GET_USER,param,BeanPropertyRowMapper.newInstance(Login.class));
    }

    @Override
    public boolean updateToken(String mail, String token) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        param.addValue("token",token);
        return jdbcTemplate.update(SqlQueries.EDIT_TOKEN,param)>0;
    }

    @Override
    public boolean updateTrId(String mail, String trId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        param.addValue("trId",trId);
        return jdbcTemplate.update(SqlQueries.EDIT_TRANSACTION,param)>0;
    }

    @Override
    public boolean updatePassword(String mail, String password) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        param.addValue("password",password);
        return jdbcTemplate.update(SqlQueries.EDIT_PASSWORD,param)>0;
    }


    @Override
    public Integer getCountDown() {
        return jdbcTemplate.queryForObject(SqlQueries.GET_COUNTDOWN, new MapSqlParameterSource(), Integer.class);
    }

    @Override
    public boolean addFeedBack (String transactionId, String message) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        param.addValue("message",message);
        return jdbcTemplate.update(SqlQueries.ADD_FEEDBACK,param)>0;
    }

    @Override
    public boolean addIssue(Issue issue) {
        return jdbcTemplate.update(SqlQueries.ADD_ISSUE,new BeanPropertySqlParameterSource(issue))>0;
    }

    @Override
    public boolean checkSession(String mail) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",mail);
        return jdbcTemplate.queryForObject(SqlQueries.GET_USER_SESSION,param,Integer.class)>0;
    }

    @Override
    public boolean editProfile(Profile profile) {

        String sql=SqlQueries.EDIT_PROFILE;
        if(profile.getUserPassword()!=null){

            sql+=",password=:userPassword ";

        }
        if(profile.getUserPhone()!=null){
            sql+=",phone=:userPhone ";
        }
        sql+="where transactionId=:trId";

        return jdbcTemplate.update(sql,new BeanPropertySqlParameterSource(profile))>0;
    }


}
