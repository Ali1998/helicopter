package dao.Impl;

import dao.CountryDao;
import dao.SqlQueries;
import model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CountryDaoImpl implements CountryDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource dataSource) {
          this.jdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Country> getCountryList() {
        return jdbcTemplate.query(SqlQueries.GET_COUNTRY_LIST,BeanPropertyRowMapper.newInstance(Country.class));
    }
}
