package dao;

public interface SqlQueries {


    //Country

    String GET_COUNTRY_LIST="select name_ru nameRu,name_en nameEn,id from country";



    //User

    String REGISTRATION="insert into user(mail,password,phone,token,full_name,country_id,transactionId) " +
            "values(:userMail,:userPassword,:userPhone,:token,:userFullName,:countryId,:trId)";

    String GET_LOGIN="select " +
            "transactionId trId,status from user where mail=:mail and password=:password and (status=1 or status=2)";

    String CHECK_LOGIN="select" +
            " count(id) from user where binary mail=:mail and binary password=:password and (status=1 or status=2)";


    String CHECK_USER="select count(id) from user " +
            "where transactionId=:transactionId and status=1";


    String CHECK_USER_BY_MAIL="select count(id) from user " +
            "where mail=:mail";



    String EDIT_PROFILE="update user set country_id=:countryId,full_name=:userFullName,work_place=:workPlace," +
            "work_experience=:workExperience,work_position=:workPosition";



    String GET_PROFILE="select u.full_name userFullName,u.mail userMail,\n" +
            "(case when u.work_experience is null then '' else u.work_experience end)  workExperience,\n" +
            "(case when u.work_place is null then '' else u.work_place end)  workPlace,\n" +
            "(case when u.work_position is null then '' else u.work_position end) workPosition,\n" +
            "(case when u.phone is null then '' else u.phone end)userPhone from user u\n" +
            "left outer join user_tariffs ut on ut.user_id=u.id  \n" +
            "where u.transactionId=:transactionId and u.status=1";





    //bax buna
     String GET_USER_TARIFF_FINISH_DATE="\n" +
            "select case when a.finish_date is null then a.created_date else a.finish_date end finishDate \n" +
            "from (select u.created_date, ut.finish_date \n" +
            "from user u left outer join user_tariffs ut on u.id=ut.user_id \n" +
            "where u.transactionId=:trId) a;";





    String GET_COUNTRY="select c.id id,c.name_en nameEn,c.name_ru nameRu from user u,country c " +
            "where u.country_id=c.id " +
            "and  u.transactionId=:transactionId and u.status=1";


    String CONFIRM_USER="select count(id) from user where mail=:mail and token=:token and (status=2 or status=1)";

    String EDIT_STATUS="update user set status=1 where mail=:mail and token=:token and (status=2 or status=1)";

    String GET_USER="select transactionId trId from user where mail=:mail and status=1";

    String EDIT_TOKEN="update user set token=:token,created_date=now() where mail=:mail and (status=1 or status=2) ";//status 1 ve ya 2 elave et

    String EDIT_TRANSACTION="update user set transactionId=:trId where mail=:mail and status=1";

    String EDIT_PASSWORD="update user set password=:password where mail=:mail and (status=1 or status=2) ";

    String GET_COUNTDOWN="select data_en " +
            "from application_datas where name='CountDown'";

    String GET_USER_SESSION="SELECT TIME_TO_SEC(timediff(now(), created_date))<(select count_down from settings) " +
            "from user where mail=:mail and (status=1 or status=2)";






    //Tariff


    String GET_PACKAGES_BY_ID="select t.id,t.amount,t.month from tariff t where t.id=:id and t.status=1";


    String GET_TARIFF_LIST="select id,amount,month from tariff where status=1 order by month asc";

    String BUY_TARIFF="insert into user_tariffs(user_id,tariff_id,finish_date,price) " +
            "values((select id from user where transactionId=:transactionId and status=1),:tariffId,now()+interval :timeCount month,:price)";



    String CHECK_USER_TARIFF="select count(ut.id) from user_tariffs ut,user u " +
            " where u.id=ut.user_id and u.transactionId=:transactionId ";



    String EDIT_BUY_TARIFF="update user_tariffs set user_id=(select id from user where transactionId=:transactionId and status=1)," +
            " tariff_id=:tariffId,finish_date=finish_date+ interval :timeCount month,price=:price+price " +
            "where user_id=(select id from user where transactionId=:transactionId and status=1)";



    //feedBack and Issue

    String ADD_FEEDBACK="insert into feedback(user_id,message) " +
            "values((select id from user where transactionId=:transactionId),:message)";


    String ADD_ISSUE="insert into callback(message,user_id,issue_id) " +
            "values(:message,(select id from user where transactionId=:transactionId),:issueId)";





    //edit and getall data


    //helicopter data

    String GET_HELICOPTER_LIST="SELECT id helicopterId,\n" +
            "(case when name_en is null then '' else name_en end) helicopterNameEn,\n" +
            "(case when name_ru is null then '' else name_ru end) helicopterNameRu\n" +
            "FROM helicopter.helicopters where status=1";

    //scheme data

    String GET_SCHEMES_DATA="select id schemaId, helicopter_id helicopterId,\n" +
            "(case when name_en is null then '' else name_en end) schemaNameEn,\n" +
            "(case when name_ru is null then '' else name_ru end) schemaNameRu,\n" +
            "parentId from helicopter_schemes where status=1";

    //scheme_part data

    String GET_SCHEME_PART_DATA="select id schemaPartId,scheme_id schemaId, \n" +
            "(case when name_ru is null then '' else name_ru end )schemaPartNameRu,\n" +
            "(case when name_en is null then '' else name_en end )schemaPartNameEn,\n" +
            "(case when description_en is null then '' else description_en end )schemaPartDescEn,\n" +
            "(case when description_ru is null then '' else description_ru end )schemaPartDescRu\n" +
            "from scheme_parts where status=1";

    //scheme_part_problems

    String GET_ISSUE_LIST="select id issueId,scheme_part_id schemaPartId,\n" +
            "(case when name_en is null then '' else name_en end) issueNameEn,\n" +
            "(case when name_ru is null then '' else name_ru end) issueNameRu,\n" +
            "(case when problem_en is null then '' else problem_en end) bodyIssueEn,\n" +
            "(case when problem_ru is null then '' else problem_ru end) bodyIssueRu,\n" +
            "(case when coment_en is null then '' else coment_en end) bodyCommentEn,\n" +
            "(case when coment_ru is null then '' else coment_ru end) bodyCommentRu,\n" +
            "(case when solve_method_en is null then '' else solve_method_en end)bodySolveEn,\n" +
            "(case when solve_method_ru is null then '' else solve_method_ru end) bodySolveRu\n" +
            "from scheme_part_problems";


    //bulletens

    String GET_BULLETENS_LIST="select id,helicopter_id helicopterId,\n" +
            "ifnull(name_en,\"\") as nameEn, \n" +
            "ifnull(name_ru,\"\") as nameRu,\n" +
            "ifnull(short_desc_en,\"\") as shortDescEn,\n" +
            "ifnull(short_desc_ru,\"\") as shortDescRu,\n" +
            "ifnull(created_date,\"\") as publishDate,\n" +
            "ifnull(related_issue_en,\"\") as modelEn,\n" +
            "ifnull(related_issue_ru,\"\") as modelRu,\n" +
            "ifnull(helicopter_parts_en,\"\") as partEn,\n" +
            "ifnull(helicopter_parts_ru,\"\") as partRu,\n" +
            "ifnull(description_en,\"\") as descEn,\n" +
            "ifnull(description_ru,\"\") as descRu\n" +
            "from bulletens where status=1";


    String GET_IMAGES_LIST="select id, scheme_part_id parentId, image imageUrl\n" +
            "from scheme_part_images where length(image)>0";



    String GET_BULLETEN_IMAGE_LIST="select id,image imageUrl,bulleten_id parentId from bulleten_images \n" +
            "where length(image)>0";




    //application data

    String APPLICATION_DATA="select ifnull(data_en,\"\") dataEn," +
            "ifnull(data_ru,\"\")dataRu " +
            "from application_datas\n" +
            "where name<>'CountDown'";



    String ADD_BULLETEN_DESC="insert into bulleten_desc(bulleten_id,description_en,created_by)\n" +
            "values(:id,:message,(select id from user where transactionId=:trId))";

    String ADD_SCHEMA_DESC="insert into scheme_part_problems_desc(problem_id,message_en,created_by)\n" +
            "values(:id,:message,(select id from user where transactionId=:trId))";














}
