package dao;
import model.Country;

import javax.sql.DataSource;
import java.util.List;

public interface CountryDao {


    void setJdbc(DataSource dataSource);
    List<Country>getCountryList();

}
