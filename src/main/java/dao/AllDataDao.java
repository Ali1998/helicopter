package dao;

import model.*;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

public interface AllDataDao {


    void                    setJdbc(DataSource dataSource);
    List<Helicopter>        getHelicopterList();
    List<Schemes>           getSchemes();
    List<SchemePart>        getSchemeParts();
    List<Issue>             getIssueList();
    List<Bulletin>          getBulletens();
    List<Images>            getImagesList();
    List<BulletinImages>    getBulletinImages();
    List<Map<String,Object>>      getApplicationDatas();
    boolean                       addDescription(SchemaBulletenDesc desc);














}
