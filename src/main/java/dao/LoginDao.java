package dao;

import model.Issue;
import model.Login;
import model.Profile;

import javax.sql.DataSource;
import java.util.Map;

public interface LoginDao {

    void setJdbc(DataSource dataSource);

    boolean addRegister(Profile profile);

    Login checkLogin(Login login);

    boolean checkUser(String transactionId);

    boolean checkMail(String mail,int type);

    Map<String,Object> getProfile(String transactionId);

    boolean confirmUser(String mail,String token);

    Login getUser(String mail);

    boolean updateToken(String mail,String token);

    boolean updateTrId(String mail,String trId);

    boolean updatePassword(String mail,String password);

    Integer getCountDown();

    boolean addFeedBack (String transactionId,String message);

    boolean addIssue(Issue issue);

    boolean checkSession(String mail);

    boolean editProfile(Profile profile);











}
