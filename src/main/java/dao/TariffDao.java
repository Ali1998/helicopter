package dao;

import model.Tariff;

import javax.sql.DataSource;
import java.util.List;

public interface TariffDao {


    void setJdbc(DataSource dataSource);

    List<Tariff>getTariffList(String transactionId);

    Tariff tariff(int trfId);

    boolean buyTariff(Tariff tariff,String transactionId,Integer id);


    String tariffDate(String transactionId);

    boolean checkUserTariff(String transactionId);





}
