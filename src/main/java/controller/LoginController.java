package controller;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.AllDataService;
import service.HttpService;
import service.LoginService;
import service.TariffService;
import util.GsonInitializer;
import util.MailService;
import util.Methods;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private HttpService service;


    @Autowired
    private MailService mailService;

    @Autowired
    private AllDataService allDataService;

    @Autowired
    private TariffService tariffService;


    private static final Logger logger = Logger.getLogger(LoginController.class);

    //finished
    @RequestMapping(value = "/helicopter/registration",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> register(@RequestBody Map<String,Object> map){

        Map<String,Object>data=new HashMap<>();
        Profile register=new Profile();
        try {
            register.setUserMail(map.get("userMail").toString());
            if(map.get("userPhone")!=null){
                register.setUserPhone(map.get("userPhone").toString());
                String phone=register.getUserPhone();
                if(phone.trim().length()>0 &&
                       !Methods.isNumber(phone)){

                    data.put("statusCode",Constant.WrongParam);
                    data.put("statusMessage","This format is wrong");
                    return data;

                }else if(phone.trim().length()>0 &&
                        phone.trim().length()<4){
                    data.put("statusCode",Constant.WrongLength);
                    data.put("statusMessage","the phone number lenth is min 4 character");
                    return data;

                }
            }
            register.setUserPassword(map.get("userPassword").toString());
            register.setUserFullName(map.get("userFullName").toString());
            register.setCountryId(Integer.parseInt(map.get("countryId").toString()));
            register.setToken(Methods.generateRandom());
            register.setTrId(UUID.randomUUID().toString());

            if(register.getUserMail().trim().length()==0 ||
                    register.getUserFullName().trim().length()==0 ||
                    register.getUserPassword().trim().length()==0){

                data.put("statusCode",Constant.EmptyData);
                data.put("statusMessage","Empty Data");

            }else if(register.getUserPassword().trim().length()<6){
                data.put("statusCode",Constant.EmptyData);
                data.put("statusMessage","Password must be min 6 character!");
            }else{
                if(loginService.checkMail(register.getUserMail(),2)){
                    data.put("statusCode",Constant.DuplicateKey);
                    data.put("statusMessage","This email already used.Please enter other email!");
                }else if(loginService.addRegister(register)){
                    data.put("statusCode",Constant.NotConfirmed);
                    data.put("statusMessage","You have successfully Register and Please you confirm your email!");
                    data.put("countdown",loginService.getCountDown());
                    mailService.sendMail(Config.SUBJECT,Config.BODY+"-"+register.getToken(),register.getUserMail());

                }
            }
        } catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);

        }catch (DataIntegrityViolationException ex){
             data.put("statusCode",Constant.NotCurrentData);
             data.put("statusMessage","This country not found!");
             ex.printStackTrace();
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            logger.info("error:"+ex);
        }
        logger.info("Request of Register : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of Register : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }


    //finished
    @RequestMapping(value = "/helicopter/login",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> login(@RequestBody Map<String,Object>map,HttpServletResponse response) {

        Map<String,Object>data=new HashMap<>();
        Login login=new Login();
        try{
            login.setMail(map.get("userMail").toString());
            login.setPassword(map.get("userPassword").toString());
            Login validation=loginService.checkLogin(login);
            String transactionId=UUID.randomUUID().toString();
            String mail=map.get("userMail").toString();

            if(validation!=null){
                if(validation.getStatus()==1){
                    data.put("statusCode",Constant.SUCCESS);
                    data.put("statusMessage","Success");
                    loginService.updateTrId(mail,transactionId);
                    response.addHeader("transactionId",transactionId);
                }else if(validation.getStatus()==2){
                    data.put("statusCode",Constant.NotConfirmed);
                    data.put("statusMessage","Please confirm your email");
                    String token=Methods.generateRandom();
                    mailService.sendMail(Config.SUBJECT,Config.BODY+"-"+token,login.getMail());
                    loginService.updateToken(login.getMail(),token);
                    data.put("countdown",loginService.getCountDown());
                }
            }else{
                if(loginService.checkMail(mail,1)){
                    data.put("statusCode",Constant.WrongPassword);
                    data.put("statusMessage","This Password is wrong!");
                }else{
                    data.put("statusCode",Constant.NoInformation);
                    data.put("statusMessage","This account has not found");
                }

        }

        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed param!");
            ex.printStackTrace();
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
        }
        logger.info("Request of validation : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of validation : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }


    //resend and forget finished
    @RequestMapping(value = "/helicopter/sendToken",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> sendToken(@RequestBody Map<String,Object>map) {

        Map<String,Object>data=new HashMap<>();
        String token=Methods.generateRandom();

        try{

            String mail=map.get("userMail").toString();

            if(mail.trim().length()==0){
                data.put("statusCode",Constant.EmptyData);
                data.put("statusMessage","Empty data");
            }else if(loginService.checkMail(mail,2)){
                mailService.sendMail(Config.SUBJECT,Config.BODY+"-"+token,mail);
                if(loginService.updateToken(mail,token)){
                     data.put("statusCode",Constant.SUCCESS);
                     data.put("statusMessage","Success");
                     data.put("countdown",loginService.getCountDown());
                }
            }else{
                data.put("statusCode",Constant.NotCurrentData);
                data.put("statusMessage","This email has not found!");
            }


         }
         catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed param!");
            ex.printStackTrace();
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
        }

        logger.info("Request of forget : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of forget : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }





    //apply token for forget
    @RequestMapping(value = "/helicopter/applyToken",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object>applyToken(@RequestBody Map<String,Object>objectMap) {

        Map<String,Object>data=new HashMap<>();

        try {

            String mail=objectMap.get("userMail").toString();
            String token=objectMap.get("userToken").toString();
            String password=objectMap.get("userPassword").toString();
            if(loginService.checkMail(mail,2)){
                if(loginService.checkSession(mail)){

                    if(password.trim().length()<6){
                        data.put("statusCode",Constant.EmptyData);
                        data.put("statusMessage","Password must be min 6 character!");
                    }else if(loginService.confirmUser(mail,token)) {

                        if (loginService.updatePassword(mail, password)) {
                            data.put("statusCode", Constant.SUCCESS);
                            data.put("statusMessage", "Success");
                            loginService.updateToken(mail, Methods.generateRandom());
                        }

                    }else{
                        data.put("statusCode", Constant.WrongCode);
                        data.put("statusMessage","your code is false");
                    }
                }else{
                    data.put("statusCode", Constant.Session);
                    data.put("statusMessage","your Session the end Please again send mail");
                }

            }else{
                data.put("statusCode",Constant.NotCurrentData);
                data.put("statusMessage","This email has not found!");
            }

        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            logger.info("error:"+ex);
            ex.printStackTrace();
        }

        logger.info("Request of applyToken: " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of applyToken : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }






    //finished

    @RequestMapping(value = "/helicopter/getProfile",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> getProfile() {

        Map<String,Object>data=new HashMap<>();
        Map<String,String>map;

        try {
            map = service.getHeadersInfo();
            String transactionId=map.get(Config.TRANSACTION);
            if(loginService.checkUser(transactionId)){
                data=loginService.getProfile(transactionId);
                data.put("finishDate",tariffService.tariffDate(transactionId));
                data.put("applicationData",allDataService.getApplicationData());
                data.put("statusCode",Constant.SUCCESS);
                data.put("statusMessage","Success");

            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }


        }catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
        }
        logger.info("Response of getProfile : " + GsonInitializer.getInstance().toJson(data));
        return data;

    }







    //finished

    @RequestMapping(value = "/helicopter/confirm",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> confirm(@RequestBody Map<String,Object>map, HttpServletResponse response) {

        Map<String,Object>data=new HashMap<>();

        try {
            String mail=map.get("userMail").toString();
            String token=map.get("userToken").toString();
            String trId=UUID.randomUUID().toString();

            if(loginService.checkMail(mail,2)){
                if(loginService.checkSession(mail)){
                    if(loginService.checkMail(mail,1)){
                        data.put("statusCode",Constant.Approved);
                        data.put("statusMessage","This user approved already!");
                    }
                    else if(loginService.confirmUser(mail,token)){
//                        Login login=loginService.getUser(mail);
                        data.put("statusCode",Constant.SUCCESS);
                        data.put("statusMessage","Success");
                        loginService.updateTrId(mail,trId);
                        response.addHeader("transactionId",trId);
                        loginService.updateToken(mail,Methods.generateRandom());
                    }
                    else{
                        data.put("statusCode", Constant.WrongCode);
                        data.put("statusMessage","Your code is false");
                    }
                }else{
                    data.put("statusCode", Constant.Session);
                    data.put("statusMessage","your Session the end Please again send mail");
                }
            }else{
                data.put("statusCode",Constant.NoInformation);
                data.put("statusMessage","This account has not found");
            }



        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            logger.info("error:"+ex);
            ex.printStackTrace();

        }

        logger.info("Request of confirm : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of confirm : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }






    @RequestMapping(value = "/helicopter/editProfile",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object>editProfile(@RequestBody Map<String,Object>map){

        Map<String,Object>data=new HashMap<>();
        Map<String,String>objectMap;
        Profile profile=new Profile();
        try{
            objectMap = service.getHeadersInfo();
            String transactionId=objectMap.get(Config.TRANSACTION);
            profile.setUserFullName(map.get("userFullName").toString());
            profile.setCountryId(Integer.parseInt(map.get("countryId").toString()));
            profile.setWorkPosition(map.get("workPosition").toString());
            profile.setWorkPlace(map.get("workPlace").toString());
            profile.setWorkExperience(map.get("workExperience").toString());
            profile.setTrId(transactionId);
            if(map.get("userPhone")!=null){
                profile.setUserPhone(map.get("userPhone").toString());
                if(!Methods.isNumber(profile.getUserPhone()) && profile.getUserPhone().trim().length()>0){
                    data.put("statusCode",Constant.WrongParam);
                    data.put("statusMessage","Number format is wrong!");
                    return data;
                }else if(profile.getUserPhone().trim().length()>0 &&
                        profile.getUserPhone().trim().length()<4){
                    data.put("statusCode",Constant.WrongLength);
                    data.put("statusMessage","the phone number lenth is min 4 character");
                    return data;

                }

            }
            if(loginService.checkUser(transactionId)){
                if(map.get("userPassword")!=null){
                    profile.setUserPassword(map.get("userPassword").toString());
                    if(profile.getUserPassword().trim().length()<6 && profile.getUserPassword().trim().length()>0){
                        data.put("statusCode",Constant.EmptyData);
                        data.put("statusMessage","Password must be min 6 character!");
                        return data;
                    }


                }
                if(profile.getUserFullName().trim().length()==0){
                    data.put("statusCode",Constant.EmptyData);
                    data.put("statusMessage","Empty Data");
                }else{
                    if(loginService.editProfile(profile)){
                        data.put("statusCode",Constant.SUCCESS);
                        data.put("statusMessage","Success");
                    }
                }

            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }



        }catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            ex.printStackTrace();
            logger.info("error:"+ex);
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
            logger.info("error:"+ex);
        }

        logger.info("Request of editProfile : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of editProfile : " + GsonInitializer.getInstance().toJson(data));
        return data;
    }




     //problemler

























}
