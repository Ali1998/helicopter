package controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Config;
import model.Constant;
import model.Tariff;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.HttpService;
import service.LoginService;
import service.TariffService;
import util.GsonInitializer;


import java.util.HashMap;
import java.util.Map;
import java.util.List;

@RestController
public class TariffController {


    @Autowired
    private TariffService tariffService;

    @Autowired
    private HttpService service;

    @Autowired
    private LoginService loginService;


    private static final Logger logger = Logger.getLogger(TariffController.class);

    @RequestMapping(value = "/helicopter/getTariffList",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> getTariffList(){

        Map<String,Object>data=new HashMap<>();
        Map<String, String> map;



        try
        {
            map = service.getHeadersInfo();
            String transactionId=map.get(Config.TRANSACTION);
            if(loginService.checkUser(transactionId)){

                List<Tariff>tariffList=tariffService.getTariffList(transactionId);
                if(tariffList==null || tariffList.isEmpty()){
                    data.put("statusCode",Constant.NoInformation);
                    data.put("statusMessage","No Information");
                }else{
                    data.put("statusCode",Constant.SUCCESS);
                    data.put("statusMessage","Success");
                    data.put("tariff",tariffList);
                }
            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }




        }catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
        }

        logger.info("Response of getTariffList : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }



    @RequestMapping(value = "/helicopter/buyPackage",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> getPckId(@RequestBody Map<String,Object>map){

        Map<String,Object>data=new HashMap<>();
        Map<String,String>objectMap;

        try{

            objectMap = service.getHeadersInfo();
            String transactionId= objectMap.get(Config.TRANSACTION);
            Integer id=Integer.parseInt(map.get("id").toString());
            if(loginService.checkUser(transactionId)){
                Tariff tariff=tariffService.tariff(id);
                if(tariff!=null){
                    if(tariffService.buyTariff(tariff,transactionId, id)){
                        data.put("statusCode",Constant.SUCCESS);
                        data.put("statusMessage","Success");
                    }
                }else{
                    data.put("statusCode",Constant.NoInformation);
                    data.put("statusMessage","There isn't package in this id");
                }

            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }

        }catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            ex.printStackTrace();
            logger.info("error:"+ex);
        }catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
        }


        logger.info("Request of buyPackage : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of buyPackage : " + GsonInitializer.getInstance().toJson(data));


       return data;
    }


}
