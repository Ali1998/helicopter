package controller;

import model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.AllDataService;
import service.HttpService;
import service.LoginService;
import service.TariffService;
import util.GsonInitializer;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MainController {

    @Autowired
    private AllDataService allDataService;

    @Autowired
    private HttpService service;

    @Autowired
    private LoginService loginService;

    @Autowired
    private TariffService tariffService;





    private static final Logger logger = Logger.getLogger(MainController.class);


    @RequestMapping(value = "/helicopter/getAllData",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> editProfile(){

        Map<String,Object>data=new HashMap<>();
        Map<String,String>map;

        try{
             map=service.getHeadersInfo();
             String transactionId=map.get(Config.TRANSACTION);
             if(loginService.checkUser(transactionId)){

                   if(!tariffService.checkUserTariff(transactionId)){
                       data.put("statusCode",Constant.NoInformation);
                       data.put("statusMessage","this user doesn't have tarrif");
                   }else{
                       List<Helicopter>getHelicopters=allDataService.getHelicopterList();
                       List<SchemePart>getSchemeParts=allDataService.getSchemeParts();
                       List<Schemes>getSchemes=allDataService.getSchemes();
                       List<Issue>getIssues=allDataService.getIssueList();
                       List<Bulletin>getBulletins=allDataService.getBulletens();
                       List<Images>images=allDataService.getImagesList();
                       List<BulletinImages> bulletinImages=allDataService.getBulletinImages();
                       data.put("statusCode",Constant.SUCCESS);
                       data.put("statusMessage","Success");
                       data.put("Helicopter",getHelicopters);
                       data.put("SchemePart",getSchemeParts);
                       data.put("Schemes",getSchemes);
                       data.put("Issue",getIssues);
                       data.put("Bulletin",getBulletins);
                       data.put("Images",images);
                       data.put("bulletinImages",bulletinImages);
                    }
             }else{
                 data.put("statusCode",Constant.WrongTransaction);
                 data.put("statusMessage","transactionId is wrong!");
             }

        } catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
            logger.info("error:"+ex);
        }

        logger.info("Response of getAllData : " + GsonInitializer.getInstance().toJson(data));
        return data;
    }






    @RequestMapping(value = "/helicopter/addDescription",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> editProfile(@RequestBody Map<String,Object>objectMap){

        Map<String,Object>data=new HashMap<>();
        Map<String,String>map;
        SchemaBulletenDesc desc=new SchemaBulletenDesc();

        try{
            map=service.getHeadersInfo();
            String transactionId=map.get(Config.TRANSACTION);
            desc.setTrId(transactionId);
            desc.setId(Integer.parseInt(objectMap.get("id").toString()));
            desc.setMessage(objectMap.get("message").toString());
            desc.setType(Integer.parseInt(objectMap.get("type").toString()));


            if(loginService.checkUser(transactionId)){

                if(allDataService.addDescription(desc)){
                    data.put("statusCode",Constant.SUCCESS);
                    data.put("statusMessage","Success");
                }

            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }

        }
        catch (DataIntegrityViolationException ex){
            data.put("statusCode",Constant.NotCurrentData);
            data.put("statusMessage","This country not found!");
            ex.printStackTrace();
        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            ex.printStackTrace();
            logger.info("error:"+ex);
        }

        logger.info("Request of addDescription: "+GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of addDescription : " + GsonInitializer.getInstance().toJson(data));
        return data;
    }









}
