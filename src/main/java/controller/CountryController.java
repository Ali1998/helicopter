package controller;

import model.Constant;
import model.Country;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.CountryService;
import util.GsonInitializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CountryController {

    @Autowired
    private CountryService countryService;


    private static final Logger logger = Logger.getLogger(CountryController.class);


    @RequestMapping(value = "/helicopter/getCountryList",method = RequestMethod.GET,produces="application/json")
    public Map<String,Object> getCountryList(){
        Map<String,Object>data=new HashMap<>();

        try {

            List<Country> countryList = countryService.getCountryList();

            if (countryList.isEmpty() || countryList == null) {
                data.put("statusCode", Constant.NoInformation);
                data.put("statusMessage", "No Information");
            } else {
                data.put("statusCode", Constant.SUCCESS);
                data.put("statusMessage","Success");
                data.put("countries", countryList);

            }

        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
            ex.printStackTrace();
        }

        logger.info("Response of getCountryList : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }




}
