package controller;

import model.Config;
import model.Constant;
import model.Issue;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.HttpService;
import service.LoginService;
import util.GsonInitializer;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FeedBackController {


     @Autowired
     private HttpService service;

     @Autowired
     private LoginService loginService;

    private static final Logger logger = Logger.getLogger(FeedBackController.class);


    @RequestMapping(value = "/helicopter/feedBack",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> addFeedback(@RequestBody Map<String,Object>map) {

        Map<String,Object>data=new HashMap<>();
        Map<String,String>objectMap;

        try {

            objectMap = service.getHeadersInfo();
            String transactionId=objectMap.get(Config.TRANSACTION);
            String text=map.get("message").toString();
            if(loginService.checkUser(transactionId)){

                if(text.trim().length()==0){
                    data.put("statusCode",Constant.EmptyData);
                    data.put("statusMessage","Empty data");
                }
                else if(loginService.addFeedBack(transactionId,text)){
                    data.put("statusCode",Constant.SUCCESS);
                    data.put("statusMessage","Success");
                }


            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }

        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            logger.info("error:"+ex);
            ex.printStackTrace();
        }

        logger.info("Request of feedBack : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of feedBack : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }


    //



    @RequestMapping(value = "/helicopter/callback",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object> addIssue(@RequestBody Map<String,Object>map) {

        Map<String,Object>data=new HashMap<>();
        Map<String,String>objectMap;


        try {

            objectMap = service.getHeadersInfo();
            String transactionId=objectMap.get(Config.TRANSACTION);
            String message=map.get("message").toString();
            Integer issueId=Integer.parseInt(map.get("issueId").toString());
            Issue issue=new Issue();
            issue.setIssueId(issueId);
            issue.setMessage(message);
            issue.setTransactionId(transactionId);
            if(loginService.checkUser(transactionId)){

                if(message.trim().length()==0){
                    data.put("statusCode",Constant.EmptyData);
                    data.put("statusMessage","Empty data");
                }
                else if(loginService.addIssue(issue)){
                    data.put("statusCode",Constant.SUCCESS);
                    data.put("statusMessage","Success");
                }
            }else{
                data.put("statusCode",Constant.WrongTransaction);
                data.put("statusMessage","transactionId is wrong!");
            }

        }
        catch (NullPointerException ex){
            data.put("statusCode",Constant.MISSED_PARAM);
            data.put("statusMessage","Missed Param");
            logger.info("error:"+ex);
        }catch (DataIntegrityViolationException ex){
            data.put("statusCode",Constant.NotCurrentData);
            data.put("statusMessage","This issue not found!");
        }
        catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System Error");
            logger.info("error:"+ex);
            ex.printStackTrace();
        }

        logger.info("Request of feedBack : " + GsonInitializer.getInstance().toJson(map));
        logger.info("Response of feedBack : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }




}
