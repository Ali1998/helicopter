package service;


import dao.LoginDao;
import model.Issue;
import model.Login;
import model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class LoginService {

    @Autowired
    private LoginDao loginDao;

    public boolean addRegister(Profile profile){return loginDao.addRegister(profile);}

    public Login checkLogin(Login login){return loginDao.checkLogin(login);}

    public boolean checkUser(String transactionId){return loginDao.checkUser(transactionId);}

    public boolean checkMail(String mail,int type){return loginDao.checkMail(mail,type);}

    public Map<String,Object> getProfile(String transactionId){return loginDao.getProfile(transactionId);}

    public boolean confirmUser(String mail, String token){return loginDao.confirmUser(mail,token);}

    public Login getUser(String mail){return loginDao.getUser(mail);}

    public boolean updateToken(String mail, String token){return loginDao.updateToken(mail,token);}

    public boolean updatePassword(String mail, String password){return loginDao.updatePassword(mail,password);}

    public Integer getCountDown(){return loginDao.getCountDown();}

    public boolean addFeedBack (String transactionId, String message){return loginDao.addFeedBack(transactionId,message);}

    public boolean addIssue(Issue issue){return loginDao.addIssue(issue);}

    public boolean checkSession(String mail){return loginDao.checkSession(mail);}

    public boolean editProfile(Profile profile){return loginDao.editProfile(profile);}

    public boolean updateTrId(String mail, String trId){return loginDao.updateTrId(mail,trId);}


}
