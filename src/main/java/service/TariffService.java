package service;


import dao.TariffDao;
import model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffService {


    @Autowired
    private TariffDao tariffDao;

    public List<Tariff> getTariffList(String transactionId){return tariffDao.getTariffList(transactionId);}

    public Tariff tariff(int trfId){return tariffDao.tariff(trfId);}

    public boolean buyTariff(Tariff tariff,String transactionId,Integer id){return tariffDao.buyTariff(tariff,transactionId,id);}

    public String tariffDate(String transactionId){return tariffDao.tariffDate(transactionId);}

    public boolean checkUserTariff(String transactionId){return tariffDao.checkUserTariff(transactionId);}


}
