package service;


import dao.AllDataDao;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AllDataService {

    @Autowired
    private AllDataDao allDataDao;


    public List<Helicopter>   getHelicopterList(){return allDataDao.getHelicopterList();}
    public List<Schemes>      getSchemes(){return allDataDao.getSchemes();}
    public List<SchemePart>   getSchemeParts(){return allDataDao.getSchemeParts();}
    public List<Issue>        getIssueList(){return allDataDao.getIssueList();}
    public List<Bulletin>     getBulletens(){return allDataDao.getBulletens();}
    public List<Images>       getImagesList(){return allDataDao.getImagesList();}
    public List<BulletinImages> getBulletinImages(){return allDataDao.getBulletinImages();}
    public ApplicationData    getApplicationData(){

        List<Map<String,Object>>map=allDataDao.getApplicationDatas();
        int count=0;
        ApplicationData data=new ApplicationData();
        for(Map<String,Object>entry:map){

                String dataEn=entry.get("dataEn").toString();
                Object dataRu=entry.get("dataRu").toString();
                switch(count){
                    case 0:
                        data.setAboutEn(dataEn);
                        data.setAboutRu(String.valueOf(dataRu));
                        break;
                    case 1:
                        data.setPhoneEn(dataEn);
                        data.setPhoneRu(String.valueOf(dataRu));
                        break;
                    case 2:
                        data.setEmailEn(dataEn);
                        data.setEmailRu(String.valueOf(dataRu));
                        break;

                }
                count++;

        }


        return data;
    }

    public boolean addDescription(SchemaBulletenDesc desc){return allDataDao.addDescription(desc);}




}
