package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


@Service
public class HttpService {

    @Autowired
   private HttpServletRequest request;

    public Map<String, String> getHeadersInfo() {

        Map<String, String> map = new HashMap<>();



        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }


        return map;
    }

}
